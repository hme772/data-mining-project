# -*- coding: utf-8 -*-
from enum import Enum

class  The_original(Enum):
      Walking = 1
      WalkingUpstairs = 2
      WalkingDownstairs = 3
      Sitting = 4
      Standing = 5
      Laying = 6
      DragLimp = 7
      JumpLimp = 8
      PersonFall = 9
      PhoneFall = 10
      Running = 11
      AustoLimp = 12

def convertNameActivity(number):
    
    if  number == 7 or number == 8 :
        act_names = 1
    elif number == 12:
        act_names = 2
    else:
        act_names = 3
    
    return act_names 
    