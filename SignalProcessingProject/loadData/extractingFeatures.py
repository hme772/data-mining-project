# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 11:35:35 2021

@author: moria
"""
import convertFromJson
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv
from sklearn import tree
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.model_selection import train_test_split
import math
from itertools import zip_longest
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.metrics import plot_confusion_matrix
from sklearn.metrics import classification_report, confusion_matrix

filename = "filterData.csv"
db=pd.read_csv(filename,error_bad_lines=False)
z = db.iloc[:,:]
m = z[['place','x','y','z','activityNumber']]
#m1 = z[['key','x','y','z','activityNumber']]
X = m.drop('activityNumber', axis=1)
Y = m['activityNumber']
Xyz=X.drop('place', axis=1)
place=m['place']

ax=m['x']
ay=m['y']
az=m['z']
atx=[]


data = z
fieldnames = ['place','x','y','z','activityNumber']
df = pd.DataFrame(m, fieldnames)
df['activityNumber'] = Y
X_train, X_test, Y_train, Y_test = train_test_split(df[fieldnames], df['activityNumber'], random_state=0)

    
#print(np.transpose(9.80665))
#print(Y)
#print(db)
 




#calculates the means of x,y,z
counter=0
mean_x_sum=0
mean_y_sum=0
mean_z_sum=0
mean_x_list=[]
mean_y_list=[]
mean_z_list=[]
 
#calculates the std of x,y,z
std_x_temp=[]#we have to send a list of objects to send to the function std so we gathered 50 objects each time
std_y_temp=[]
std_z_temp=[]
std_x_list=[]
std_y_list=[]
std_z_list=[]
activity_list=[]


temp=0
for i in range(len(ax)):
    if temp<place[i]:
        counter+=1
        mean_x_sum+=ax[i]
        std_x_temp.append(ax[i])
 
        if counter==50:
            mean_x_list.append(mean_x_sum/50)
            std_x_list.append(np.std(std_x_temp))
            activity_list.append(Y[i]) #determines the disease of each speciman
            mean_x_sum=counter=0
            std_x_temp=[]
            temp=place[i]
       # i+=1
    else:
        mean_x_sum=ax[i]
        counter=1
        std_x_temp=[]
        std_x_temp.append(ax[i])
        temp=place[i]
   

counter=0
temp=0
for i in range(len(ay)):
    
    if temp<place[i]:
        counter+=1
        mean_y_sum+=ay[i]
        std_y_temp.append(ay[i])
 
        if counter==50:
            mean_y_list.append(mean_y_sum/50)
            std_y_list.append(np.std(std_y_temp))
            mean_y_sum=counter=0
            std_y_temp=[]
            temp=place[i]
    else:
        mean_y_sum=ay[i]
        counter=1
        std_y_temp=[]
        std_y_temp.append(ay[i])
        temp=place[i] 


counter=0
temp=0
for i in range(len(az)):
    
    if temp<place[i]:
        counter+=1
        mean_z_sum+=az[i]
        std_z_temp.append(az[i])
 
        if counter==50:
            mean_z_list.append(mean_z_sum/50)
            std_z_list.append(np.std(std_z_temp))
            mean_z_sum=counter=0
            std_z_temp=[]
            temp=place[i]
    else:
        mean_z_sum=az[i]
        counter=1
        std_z_temp=[]
        std_z_temp.append(az[i])
        temp=place[i] 

        
        

#calculates the distance between 2 points
counter=0
temp=0
p1=[]
p2=[]
distance_list=[]      
for i in range(len(Xyz)):
    if temp<place[i]:
        if counter==0 and i==0:
            p1.append(ax[i])
            p1.append(ay[i])
            p1.append(az[i])
        counter+=1
        if counter==50:
            p2.append(ax[i])
            p2.append(ay[i])
            p2.append(az[i])
            distance_list.append(math.dist(p1, p2))
            counter=0
            p1=[]
            p1.append(p2[0])
            p1.append(p2[1])
            p1.append(p2[2])
            p2=[]
    else:
        temp=place[i]
        p1=[]
        p2=[]
        p1.append(ax[i])
        p1.append(ay[i])
        p1.append(az[i])
        counter=0

features=[]
with open('features.csv', 'w', newline='') as csvfile:
    fieldnames = ['mean_x','mean_y','mean_z','std_x','std_y','std_z','distance']
    writer = csv.writer(csvfile)
    writer.writerow(fieldnames)
    for i in range(len(mean_x_list)):  
        d2=[mean_x_list[i],mean_y_list[i],mean_z_list[i],
            std_x_list[i],std_y_list[i],std_z_list[i],
             distance_list[i]] 
      #  features = zip_longest(*d2, fillvalue = '')
        writer.writerow(d2)


filename = "features.csv"
db=pd.read_csv(filename,error_bad_lines=False)
z = db.iloc[:,:]
features = z[['mean_x','mean_y','mean_z','std_x','std_y','std_z','distance']]
X_train, X_test, Y_train, Y_test = train_test_split(features, activity_list,test_size=0.2, random_state=1)

    
clf = tree.DecisionTreeClassifier()
clf.max_depth = 5
clf.criterion = 'entropy'
clf.fit(features, activity_list)
y_pred = clf.predict(X_test)
print(confusion_matrix(Y_test, y_pred))
print(classification_report(Y_test, y_pred))


print("DecisionTree: ")
accuracy = cross_val_score(clf, features, activity_list, scoring='accuracy', cv=10)
print("Average Accuracy of DT with depth ", clf.max_depth, " is: ", round(accuracy.mean(),3))
#tree.plot_tree(clf);
#plt.show();


cm=metrics.confusion_matrix(Y_test, y_pred, labels=[1,2,3])
matrix=metrics.plot_confusion_matrix(clf,X_test,Y_test)
matrix.ax_.set_title('Confusion Matrix',color='white')
plt.xlabel('Predicted Label',color='white')
plt.ylabel('True Label',color='white')
plt.show()


fn=['mean_x','mean_y','mean_z','std_x','std_y','std_z','distance']
cn=['StrokeLimp', 'AustoLimp', 'Proper']
fig, axes = plt.subplots(nrows = 1,ncols = 1,figsize = (10,10), dpi=350)
tree.plot_tree(clf,
               feature_names = fn, 
               class_names=cn,
               filled = True);
fig.savefig('imageD5.png')#creates the tree for depth 5
