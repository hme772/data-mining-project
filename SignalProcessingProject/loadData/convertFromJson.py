# -*- coding: utf-8 -*-
import csv
from utilities import ActivityLabels
from utilities import convertNameActivity
import json
import numpy as np
from itertools import zip_longest
#holds the data as 2 lists, actid=the number of the act =1/2/3
#totalacc is a matrix of X,Y,Z that will be transposed later
class d:
    actid=[],
    totalacc=[]
    def __init__(self, actid, totalacc):  
        self.actid = actid  
        self.totalacc = totalacc
#Function convertFromJson

# This file takes the default from-json table that saved in "dataFromWeb" and convert it to
# more fitted matrix and save it in "myFormatData.mat"
##
# The frequency
fs=50
# The acts
actnames=[]
for l in ActivityLabels.ActivityLabels:
    actnames.append(l.name)
actlabels= actnames#gets the names of the acts, david did it so we did it too,might be handy later on

#opens the csv and writes the fitting lables to the columns 
with open('dataWithoutArray.csv', 'w', newline='') as csvfile:
    fieldnames = ['key','place','timestamp','x','y','z','activityNumber']
    writer = csv.writer(csvfile)
    writer.writerow(fieldnames)

#opens the json for reading only
j=open("dataFromWeb.json", "r")
j_d=dict(json.loads(j.read()))#we decided to put the data in a form of a dictionary for convenience

#keys_outer is a list of the keys in the json
keys_outer=list(j_d.keys())
#obj_outer is a specipic key 
for obj_outer in keys_outer:
    length=len(j_d[obj_outer])
    #we must have empty lists or the rest of the code won't work
    idvec=[]
    accvec=[]
    data=[]
    key=[]
    place=[]
    time=[]
    
    
    for i in range(length):
        key.append(obj_outer)#we repeat the key for every record because the csv is a table
        place.append(i) #to find the place in the key
        temp=dict(j_d[obj_outer][i])       
        idvec.append(convertNameActivity.convertNameActivity(int(temp["activityNumber"])))#activity numbers
        temaccarr=temp["values"]#the values
        time.append(temp["timestamp"])#the timestamp
        accvec.append(temaccarr)
        #print((j_d[obj_outer][i]))#when the printing ends the csv is finished
        #find out how to split a list
    data.append(d(idvec,accvec))
   # print(str(data[0].actid[0]) +' '+str(data[0].totalacc[0]))
    
   # the writing
    with open('dataWithoutArray.csv', 'a', newline='') as csvfile:
         writer = csv.writer(csvfile)
         for item in data:
             trans=np.transpose(item.totalacc)#turns the values matrix to a c-transpose so will be able to get X Y Z separated
             #trans[0]=X,trans[1]=Y,trans[2]=Z
             d2 = [key,place,time,trans[0],trans[1],trans[2],item.actid]
            #zips all the lists together
             export_data = zip_longest(*d2, fillvalue = '')
             writer.writerows(export_data)#writes by rows
    #clears the lists for the next round
    idvec.clear()
    accvec.clear()
    key.clear()
    place.clear()
    time.clear()
       
print("done")