# -*- coding: utf-8 -*-
"""
Created on Sun Mar 21 11:53:11 2021

@author: moria
"""

import csv
import pandas as pd
import math
from utilities import ActivityLabels
from utilities import convertNameActivity
import json
import numpy as np
from itertools import zip_longest
from sklearn.model_selection import cross_val_score
from sklearn import tree
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import plot_confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.metrics import plot_confusion_matrix

#holds the data as 2 lists, actid=the number of the act =1/2/3
#totalacc is a matrix of X,Y,Z that will be transposed later

filename = "filterData.csv"
db=pd.read_csv(filename,error_bad_lines=False)
z = db.iloc[:,:]
m = z[['place','x','y','z','activityNumber']]
#m1 = z[['key','x','y','z','activityNumber']]
X = m.drop('activityNumber', axis=1)
Y = m['activityNumber']
Xyz=X.drop('place', axis=1)
place=m['place']

ax=m['x']
ay=m['y']
az=m['z']
atx=[]


data = z
fieldnames = ['place','x','y','z','activityNumber']
df = pd.DataFrame(m, fieldnames)
df['activityNumber'] = Y
X_train, X_test, Y_train, Y_test = train_test_split(df[fieldnames], df['activityNumber'], random_state=0)


#calculates the means of x,y,z
counter=0
mean_x_sum=0
mean_y_sum=0
mean_z_sum=0
mean_x_list=[]
mean_y_list=[]
mean_z_list=[]
 
#calculates the std of x,y,z
std_x_temp=[]#we have to send a list of objects to send to the function std so we gathered 50 objects each time
std_y_temp=[]
std_z_temp=[]
std_x_list=[]
std_y_list=[]
std_z_list=[]
activity_list=[]


temp=0
for i in range(len(ax)):
    if temp<place[i]:
        counter+=1
        mean_x_sum+=ax[i]
        std_x_temp.append(ax[i])
 
        if counter==50:
            mean_x_list.append(mean_x_sum/50)
            std_x_list.append(np.std(std_x_temp))
            activity_list.append(Y[i]) #determines the disease of each speciman
            mean_x_sum=counter=0
            std_x_temp=[]
            temp=place[i]
       # i+=1
    else:
        mean_x_sum=ax[i]
        counter=1
        std_x_temp=[]
        std_x_temp.append(ax[i])
        temp=place[i]
   

counter=0
temp=0
for i in range(len(ay)):
    
    if temp<place[i]:
        counter+=1
        mean_y_sum+=ay[i]
        std_y_temp.append(ay[i])
 
        if counter==50:
            mean_y_list.append(mean_y_sum/50)
            std_y_list.append(np.std(std_y_temp))
            mean_y_sum=counter=0
            std_y_temp=[]
            temp=place[i]
    else:
        mean_y_sum=ay[i]
        counter=1
        std_y_temp=[]
        std_y_temp.append(ay[i])
        temp=place[i] 


counter=0
temp=0
for i in range(len(az)):
    
    if temp<place[i]:
        counter+=1
        mean_z_sum+=az[i]
        std_z_temp.append(az[i])
 
        if counter==50:
            mean_z_list.append(mean_z_sum/50)
            std_z_list.append(np.std(std_z_temp))
            mean_z_sum=counter=0
            std_z_temp=[]
            temp=place[i]
    else:
        mean_z_sum=az[i]
        counter=1
        std_z_temp=[]
        std_z_temp.append(az[i])
        temp=place[i] 

        
        

#calculates the distance between 2 points
counter=0
temp=0
p1=[]
p2=[]
distance_list=[]      
for i in range(len(Xyz)):
    if temp<place[i]:
        if counter==0 and i==0:
            p1.append(ax[i])
            p1.append(ay[i])
            p1.append(az[i])
        counter+=1
        if counter==50:
            p2.append(ax[i])
            p2.append(ay[i])
            p2.append(az[i])
            distance_list.append(math.dist(p1, p2))
            counter=0
            p1=[]
            p1.append(p2[0])
            p1.append(p2[1])
            p1.append(p2[2])
            p2=[]
    else:
        temp=place[i]
        p1=[]
        p2=[]
        p1.append(ax[i])
        p1.append(ay[i])
        p1.append(az[i])
        counter=0

features=[]
with open('features.csv', 'w', newline='') as csvfile:
    fieldnames = ['mean_x','mean_y','mean_z','std_x','std_y','std_z','distance']
    writer = csv.writer(csvfile)
    writer.writerow(fieldnames)
    for i in range(len(mean_x_list)):  
        d2=[mean_x_list[i],mean_y_list[i],mean_z_list[i],
            std_x_list[i],std_y_list[i],std_z_list[i],
             distance_list[i]] 
      #  features = zip_longest(*d2, fillvalue = '')
        writer.writerow(d2)


filename = "features.csv"
db=pd.read_csv(filename,error_bad_lines=False)
z = db.iloc[:,:]
features = z[['mean_x','mean_y','mean_z','std_x','std_y','std_z','distance']]
X_train, X_test, Y_train, Y_test = train_test_split(features, activity_list,test_size=0.2, random_state=1)

    
clf = tree.DecisionTreeClassifier()
clf.max_depth = 5
clf.criterion = 'entropy'
clf.fit(features, activity_list)
y_pred = clf.predict(X_test)
#print(confusion_matrix(Y_test, y_pred))
#print(classification_report(Y_test, y_pred))


print("DecisionTree: ")
accuracy = cross_val_score(clf, features, activity_list, scoring='accuracy', cv=10)
print("Average Accuracy of DT with depth ", clf.max_depth, " is: ", round(accuracy.mean(),3))
#tree.plot_tree(clf);
#plt.show();


print("KNN: ")
clf2 = KNeighborsClassifier(n_neighbors=5)
clf2.fit(features, activity_list)
accuracy2 = cross_val_score(clf2, features, activity_list, scoring='precision_weighted', cv=10)
print("Average Accuracy of Knn is: ", round(accuracy2.mean(),3))


print("LOG: ")
clf4 = LogisticRegression(solver='liblinear', C=10.0, random_state=0)
clf4.fit(features, activity_list)
accuracy4 = cross_val_score(clf4, features, activity_list, scoring='accuracy', cv=10)
print("Average Accuracy of LOG is: ", round(accuracy4.mean(),3))


print("NB: ")
clf5 = GaussianNB()
clf5.fit(features, activity_list)
accuracy5 = cross_val_score(clf5, features, activity_list, scoring='accuracy', cv=10)
print("Average Accuracy of NB is: ", round(accuracy5.mean(),3))


print("RandomForest: ")

features, activity_list= make_classification(n_samples=1000, n_features=4,
                           n_informative=2, n_redundant=0,
                           random_state=0, shuffle=False)

clf = RandomForestClassifier(max_depth=5, random_state=0)
clf.fit(features, activity_list)
accuracy = cross_val_score(clf, features, activity_list, scoring='accuracy', cv=10)

print("Average Accuracy of RF with depth ", clf.max_depth, " is: ", round(accuracy.mean(),3))

