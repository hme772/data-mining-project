# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 12:23:44 2020

@author: moria
"""


#those lines of code takes the data from the FB JSON file 
import requests
import json
#import convertFromJson
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv
from sklearn import tree
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import plot_confusion_matrix
from sklearn.model_selection import train_test_split





response = json.loads(requests.get("https://collectsensorsdata.firebaseio.com/data/fs-50-v2/samples/rightPocket/accelerometer.json").content)
with open("dataFromWeb.json", "w") as data:
    json.dump(response,data)
print("end Json")
#convertFromJson()

"""
filename = "filterData.csv"
db=pd.read_csv(filename,error_bad_lines=False)
z = db.iloc[1:,1:]
m = z[['place','x','y','z','activityNumber']]
X = m.drop('activityNumber', axis=1)
Y = m['activityNumber']
XYZ=X.drop('place', axis=1)


data = z
fieldnames = ['place','x','y','z','activityNumber']
df = pd.DataFrame(m, fieldnames)
df['activityNumber'] = Y
X_train, X_test, Y_train, Y_test = train_test_split(df[fieldnames], df['activityNumber'], random_state=0)

    

clf = tree.DecisionTreeClassifier()
#clf=RandomForestClassifier(n_estimators=100)


clf.max_depth = 5
clf.fit(X, Y)

print("DecisionTree: ")
accuracy = cross_val_score(clf, X, Y, scoring='accuracy', cv=10)
print("Average Accuracy of DT with depth ", clf.max_depth, " is: ", round(accuracy.mean(),3))
#tree.plot_tree(clf);
#plt.show();

fn=['place','x','y','z']
cn=['StrokeLimp', 'AustoLimp', 'Proper']
#fig, axes = plt.subplots(nrows = 1,ncols = 1,figsize = (40,40), dpi=350)
#tree.plot_tree(clf,
#               feature_names = fn, 
#               class_names=cn,
#               filled = True);
#fig.savefig('imagename2.png')
#text_representation = tree.export_text(clf)
#print(text_representation)


"""

